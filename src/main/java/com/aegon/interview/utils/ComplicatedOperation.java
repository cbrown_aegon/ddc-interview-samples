package com.aegon.interview.utils;

import com.aegon.interview.tasks.SimpleCache;

/**
 * The Class ComplicatedOperation.
 * <p>
 * This class is simply used as part of the {@link SimpleCache} example. It's purpose is only demonstrate an operation
 * that is time consuming.
 */
public class ComplicatedOperation {

    /**
     * Instantiates a new complicated operation.
     */
    private ComplicatedOperation() {

    }

    /**
     * Calcuate.
     * 
     * @param value
     *            the value
     * @return the complicated operation
     */
    public static ComplicatedOperation calcuate(Long value) {

        try {
            System.out.println("Starting some complex operation");
            Thread.sleep(1000);
            System.out.println("Completed some complex operation");
        } catch (InterruptedException e) {

        }
        return new ComplicatedOperation();
    }
}
