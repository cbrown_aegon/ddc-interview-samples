package com.aegon.interview.tasks;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

/**
 * The Class StringConcatenation.
 * <p>
 * Can you implement the join method?
 * <p>
 * Add additional test cases to prove your implementation.
 */
public class StringConcatenation {

    /**
     * Joins the values in the array, separating them with the separator.
     * 
     * @param values
     *            the values to join
     * @param separator
     *            the separator
     * @return the joined string
     */
    public static String join(String[] values, String separator) {
        return null;
    }

    //
    // ========================================================================================
    // ========================================================================================
    // ========================================================================================
    //

    /**
     * When join called with values then joined string returned.
     */
    @Test
    public void whenJoinCalledWithValuesThenJoinedStringReturned() {

        assertEquals(
            StringConcatenation.join(new String[] { "a", "b", "c" }, ":"),
            "a:b:c");
    }
}
