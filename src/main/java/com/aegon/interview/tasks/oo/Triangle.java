package com.aegon.interview.tasks.oo;

/**
 * The Class Triangle.
 */
public class Triangle {

    /** The base. */
    private final int base;

    /** The height. */
    private final int height;

    /** The colour. */
    private final Colour colour;

    /**
     * Instantiates a new triangle.
     * 
     * @param colour
     *            the colour
     * @param base
     *            the base
     * @param height
     *            the height
     */
    public Triangle(Colour colour, int base, int height) {
        this.colour = colour;
        this.base = base;
        this.height = height;
    }

    /**
     * Gets the area.
     * 
     * @return the area
     */
    public int getArea() {
        return (base * height) / 2;
    }

    /**
     * Gets the colour.
     * 
     * @return the colour
     */
    public Colour getColour() {
        return colour;
    }
}
