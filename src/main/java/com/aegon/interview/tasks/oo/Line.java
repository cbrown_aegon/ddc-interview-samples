package com.aegon.interview.tasks.oo;

/**
 * The Class Line.
 */
public class Line {

    /** The colour. */
    private final Colour colour;

    /** The length. */
    private final int length;

    /**
     * Instantiates a new line.
     * 
     * @param colour
     *            the colour
     * @param length
     *            the length
     */
    public Line(Colour colour, int length) {
        this.colour = colour;
        this.length = length;
    }

    /**
     * Gets the length.
     * 
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * Gets the colour.
     * 
     * @return the colour
     */
    public Colour getColour() {
        return colour;
    }
}
