package com.aegon.interview.tasks.oo;

/**
 * The Class Square.
 */
public class Square {

    /** The colour. */
    private final Colour colour;

    /** The length. */
    private final int length;

    /**
     * Instantiates a new square.
     * 
     * @param colour
     *            the colour
     * @param length
     *            the length
     */
    public Square(Colour colour, int length) {
        this.colour = colour;
        this.length = length;
    }

    /**
     * Gets the area.
     * 
     * @return the area
     */
    public int getArea() {
        return length * length;
    }

    /**
     * Gets the colour.
     * 
     * @return the colour
     */
    public Colour getColour() {
        return colour;
    }
}
