package com.aegon.interview.tasks.oo;

/**
 * The Enum Colour.
 */
public enum Colour {

    /** The red. */
    RED,

    /** The green. */
    GREEN,

    /** The blue. */
    BLUE,

    /** The yellow. */
    YELLOW;
}
