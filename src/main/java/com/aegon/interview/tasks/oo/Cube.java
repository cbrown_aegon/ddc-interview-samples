package com.aegon.interview.tasks.oo;

/**
 * The Class Cube.
 */
public class Cube {

    /** The colour. */
    private final Colour colour;

    /** The length. */
    private final double length;

    /**
     * Instantiates a new cube.
     * 
     * @param colour
     *            the colour
     * @param length
     *            the length
     */
    private Cube(Colour colour, int length) {
        this.colour = colour;
        this.length = length;
    }

    /**
     * Gets the volume.
     * 
     * @return the volume
     */
    public double getVolume() {
        return length * length * length;
    }

    /**
     * Gets the colour.
     * 
     * @return the colour
     */
    public Colour getColour() {
        return colour;
    }
}
