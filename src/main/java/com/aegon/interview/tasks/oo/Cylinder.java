package com.aegon.interview.tasks.oo;

/**
 * The Class Cylinder.
 */
public class Cylinder {

    /** The colour. */
    private final Colour colour;

    /** The height. */
    private final double height;

    /** The radius. */
    private final double radius;

    /**
     * Instantiates a new cylinder.
     * 
     * @param colour
     *            the colour
     * @param height
     *            the height
     * @param radius
     *            the radius
     */
    public Cylinder(Colour colour, double height, double radius) {
        this.colour = colour;
        this.height = height;
        this.radius = radius;
    }

    /**
     * Gets the volume.
     * 
     * @return the volume
     */
    public double getVolume() {
        return (3.14d * (radius * radius)) * height;
    }

    /**
     * Gets the colour.
     * 
     * @return the colour
     */
    public Colour getColour() {
        return colour;
    }
}
