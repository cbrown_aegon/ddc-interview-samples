package com.aegon.interview.tasks.oo;

/**
 * The Class Shape.
 * <p>
 * The package com.aegon.interview.tasks.oo contains a number of shapes, some 2 dimensional, some 3 dimensional.
 * <p>
 * Please refactor these classes into a better object model, making use of best object oriented principles.
 * <p>
 * Consider the operations available, any common properties and how these objects relate to each other in the real
 * world.
 * <p>
 * You can assume the calculations contained within them are correct (although i've not tested them!). This exercise is
 * purely about OO principles so <b>do not</b> bother creating test cases. Please feel free to create any number of
 * additional classes / interfaces as required.
 * <p>
 * <b>NOTE: the constructors to each class must stay the same.</b>
 */
public class Shape {

}
