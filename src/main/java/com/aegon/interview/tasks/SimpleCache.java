package com.aegon.interview.tasks;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.util.HashMap;

import org.testng.annotations.Test;

import com.aegon.interview.utils.ComplicatedOperation;

/**
 * The Class SimpleCache.
 * <p>
 * This class provides a simple caching mechanism in an attempt to reduce the number of calls to the operation
 * 'calculate' on {@link ComplicatedOperation}. For the purpose of this exercise, we do not care what
 * {@link ComplicatedOperation} actually does, simply that it is a costly operation we want to prevent.
 * <p>
 * HINT: the static nature of this cache suggests it may be accessed by several clients.
 * <p>
 * What is wrong with this implementation? List your observations here:
 * <ul>
 * <li>...</li>
 * <li>...</li>
 * </ul>
 * <p>
 * Now, implement your code review changes.
 * <p>
 * Can you update the code to be generic, allowing any key and value pair?
 */
public class SimpleCache {

    /** The cache. */
    private static HashMap<Long, ComplicatedOperation> cache =
        new HashMap<Long, ComplicatedOperation>();

    /**
     * Gets a value from the cache or adds a new entry and returns it.
     * 
     * @param key
     *            the key
     * @return the cached operation
     */
    public static ComplicatedOperation getOrCreate(long key) {

        ComplicatedOperation operation = cache.get(new Long(key));

        if (operation == null) {

            operation = ComplicatedOperation.calcuate(key);
            cache.put(new Long(key), operation);
        }
        return operation;
    }

    /**
     * Returns the current size of the cache
     * 
     * @return the size
     */
    public static int size() {
        return cache.size();
    }

    //
    // ========================================================================================
    // ========================================================================================
    // ========================================================================================
    //

    /**
     * When cache is empty and create called then expect entry to be added.
     */
    @Test
    public void whenCacheIsEmptyAndCreateCalledThenExpectEntryToBeAdded() {
        assertEquals(SimpleCache.size(), 0);
        SimpleCache.getOrCreate(1l);
        assertEquals(SimpleCache.size(), 1);
    }

    /**
     * When value is cached and get called then expect cached value to be returned.
     */
    @Test
    public void whenValueIsCachedAndGetCalledThenExpectCachedValueToBeReturned() {

        ComplicatedOperation result = SimpleCache.getOrCreate(1l);
        assertNotNull(result);
        assertEquals(SimpleCache.size(), 1);
    }
}