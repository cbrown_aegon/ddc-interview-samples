package com.aegon.interview.tasks;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

/**
 * The Class Calculate.
 * <p>
 * How would you implement the calculate method?
 * <p>
 * HINT: Consider using a Stack.
 * <p>
 * Add additional test cases to prove your solution.
 */
public class Calculate {

    /**
     * Instantiates a new calculate.
     */
    private Calculate() {

    }

    /**
     * Calculate.
     * <p>
     * Parses the 'value' into individual characters. Each time an operation character (+, *) is found, then perform
     * that operation on the previous 2 characters. Repeats until no further characters exist and return the result.
     * 
     * @param value
     *            the value
     * @return the string
     */
    public static String calculate(String value) {

        return null;
    }

    /**
     * When calculate called then the correct value is returned.
     */
    @Test
    public void whenCalculateCalledThenTheCorrectValueIsReturned() {

        assertEquals("3", Calculate.calculate("1 2 +"));
        assertEquals("2", Calculate.calculate("1 2 *"));
        assertEquals("10", Calculate.calculate("1 2 + 3 4 + +"));
        assertEquals("14", Calculate.calculate("1 2 * 3 4 * +"));
        assertEquals("21", Calculate.calculate("1 2 + 3 4 + *"));
    }
}
